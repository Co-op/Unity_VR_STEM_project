using UnityEngine;
using System.Collections;

public class Autowalk : MonoBehaviour 
{

	public double thresholdAngle;
	public float speed;
	public bool isWalking = false;
	private Camera mainCam;
	private const int RIGHT_ANGLE = 90;


	void Start()
	{
		UpdateCam ();
	}

	void UpdateCam()
	{
		mainCam = Camera.main;
	}


	void Update () 
	{
		if (!isWalking &&
			mainCam.transform.eulerAngles.x >= thresholdAngle &&
			mainCam.transform.eulerAngles.x <= RIGHT_ANGLE)
		{
			isWalking = true;
		}

		else if (isWalking &&
				(mainCam.transform.eulerAngles.x <= thresholdAngle ||
				mainCam.transform.eulerAngles.x >= RIGHT_ANGLE))
		{
			isWalking = false;
		} 
			

		if (isWalking) {
			Vector3 direction = new Vector3(this.transform.forward.x, 0, this.transform.forward.z).normalized * speed * Time.deltaTime;
			Quaternion rotation = Quaternion.Euler(new Vector3(0, mainCam.transform.rotation.eulerAngles.y, 0));
			transform.Translate(rotation * direction);

			}

		}
		
}
